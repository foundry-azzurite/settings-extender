# 1.2.3

* Preemptive Foundry 14 compatibility

# 1.2.2

* Foundry 11 compatibility

# 1.2.1

* Foundry 10 compatibility

# 1.2.0

* Remove semver build metadata from version so Foundry version comparison doesn't shit itself
* No changes otherwise at all

# 1.1.6+0.8.6

* Fix to module system variable exporting

# 1.1.5+0.8.6

* Change compatible core version to 0.8.6

# 1.1.5

* Change the library to be a registered module library and be able to be declared as dependency

# 1.1.4

* 0.8.5 compatibility

# 1.1.3

* 0.6.1 compatibility

# 1.1.2

* 0.5.4 compatibility

# 1.1.1

* Extend `SettingsConfig` properly by adding a `baseApplication` = `SettingsConfig` option
* Make sure to always load the latest version of the library

# 1.1.0

* Add `DirectoryPicker` type
