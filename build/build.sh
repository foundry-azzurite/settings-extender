#!/usr/bin/env sh
set -o errexit
set -o xtrace

base=$(pwd)

module=$(jq -r ".name" ${base}/package.json)
version=$(jq -r ".version" ${base}/package.json)
rm -rf dist
mkdir -p dist/${module}
cd dist
cp -r ${base}/src/* ${module}

sed -i -e "s/\/\*VERSION_MARKER_START\*\/\`0.0.0\`\/\*VERSION_MARKER_END\*\//\`${version}\`/" ${module}/settings-extender.js

build_ref=${CI_COMMIT_REF_NAME}
baseUrl="https://gitlab.com/foundry-azzurite/${module}/-/jobs/artifacts/${build_ref}/raw/dist/"
suffix="?job=build"
distributionJson="{manifest: \"${baseUrl}${module}/module.json${suffix}\", download: \"${baseUrl}${module}.zip${suffix}\"}"
jq -s "( \
         .[0] | \
         { name, description, author, version } | \
         .id = .name | \
         del(.name) |\
         .authors = [ .author ] |\
         del(.author) \
       ) \
       * .[1] \
       * ${distributionJson}" \
      ${base}/package.json \
      ${base}/resources/module.json \
      > ${module}/module.json

zip ${module}.zip -r ${module}
cd -
